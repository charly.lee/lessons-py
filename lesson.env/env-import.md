# import lib in different env

```
pip install MySQL-python

# or 

pip install pymysql
```

```py
try:
    import MySQLdb
except ImportError:
    import pymysql as MySQLdb
```
