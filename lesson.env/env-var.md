# env var

## set env var

windows cmd

```
set PY_ENV=development

echo %PY_ENV%
```

powershell

```
$env:PY_ENV = "development"

$Env:PY_ENV
```

linux

```
export PY_ENV="production"

echo "$PY_ENV"
```

## read env-var

```py

import os

py_env = 'development'
if 'PY_ENV' in os.environ:
    py_env = os.environ.get('PY_ENV', 'development')
if py_env != 'production':
    py_env = 'development'

path_01 = '/path/to/dev'

if py_env == 'production':
    path_01 = '/path/to/prod'
```
